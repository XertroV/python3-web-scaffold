import logging
logging.getLogger().setLevel(logging.INFO)
logging.info("Set logging info.")


from tornado.web import Application
import tornado.ioloop

import ibdd


def make_app():
    return Application(ibdd.routes, debug=True)


if __name__ == "__main__":
    app = make_app()
    app.listen(ibdd.config.PORT)
    logging.info('Starting server on port %s' % ibdd.config.PORT)
    tornado.ioloop.IOLoop.current().start()
    logging.info('Shutting Down Server')
