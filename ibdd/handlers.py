from tornado.web import RequestHandler, HTTPError


from .db import db


class BaseHandler(RequestHandler):
    def set_default_headers(self, *args, **kwargs):
        self.set_header(r'Access-Control-Allow-Origin', r'*')
        self.set_header(r'Access-Control-Allow-Headers', r'Content-Type')

    def options(self, *args, **kwargs):
        pass


class InfoHandler(BaseHandler):
    def get(self, *args, **kwargs):
        self.write({
            'status': 'running'
        })