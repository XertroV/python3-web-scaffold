from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (
    Column,
    Integer,
    String,
    Boolean,
    ForeignKey,
    DateTime,
    Sequence,
    Float
)
import datetime


Base = declarative_base()


class Book(Base):
    __tablename__  = "books"  # matches the name of the actual database table
    id = Column(Integer, Sequence('book_seq'), primary_key=True)  # plays nice with all major database engines
    name = Column(String(50))                                    # string column need lengths
    author_id = Column(Integer)  # assumes there is a table in the database called 'authors' that has an 'id' column
    price = Column(Float)
    date_added = Column(DateTime, default=datetime.datetime.now)  # defaults can be specified as functions
    promote = Column(Boolean,default=False)