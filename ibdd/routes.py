from tornado.web import RedirectHandler, StaticFileHandler

from .handlers import *

routes = [
    (r"/", RedirectHandler, {"url": "/static/html/index.html"}),
    (r"/static/(.*)", StaticFileHandler, {'path': 'static'}),
    (r"/api0/info", InfoHandler)
]