import asyncio
import time
import logging


async def manage_scheduled_tasks():
    asyncio.ensure_future(backup_database())


async def _generic_repeating_task(f, period: int):
    assert asyncio.iscoroutinefunction(f)
    assert period >= 10
    logging.info("Scheduled %s, with period %d" % (f, period))
    while True:
        await asyncio.sleep(period - (int(time.time()) % period))
        logging.info("Running %s, with period %d" % (f, period))
        await f()
        logging.info("Completed %s, with period %d" % (f, period))
        await asyncio.sleep(2)  # sleep to ensure we don't trigger this twice in one period


async def backup_db_task():
    logging.info("Beginning Backup...")
    # members = await db.get_all_members()
    # raw_members = json_util.dumps(members).encode()
    # compressed_members = base64.b64encode(zlib.compress(raw_members, 9)).decode()
    # logging.info("Backing up users. N: %d, len(raw_members): %d, len(compressed): %d" % (len(members), len(raw_members), len(compressed_members)))
    # await email.send_backup_email(compressed_members)
    # logging.info("Backup email sent")

async def backup_database():
    pass
    # await _generic_repeating_task(backup_db_task, config['BACKUP_PERIOD'])
