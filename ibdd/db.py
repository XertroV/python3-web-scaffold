from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.engine import create_engine

from .models import *
from .config import config


engine = create_engine(config.DATABASE_URL, echo=config.debug)
db = scoped_session(sessionmaker())
db.configure(bind=engine)

Base.metadata.create_all(engine)


def test_query():
    some_books = db.query(Book)

    for book in some_books:
        print(book.name)